import json
import socket
import struct
from re import search

from models import VirtualMachine

class AppService:
    
    tasks = [
        {
            "id": 1,
            "name": "task1",
            "description": "This is task 1"
        },
        {
            "id": 2,
            "name": "task2",
            "description": "This is task 2"
        },
        {
            "id": 3,
            "name": "task3",
            "description": "This is task 3"
        }
    ]
    

    def __init__(self):
        self.tasksJSON = json.dumps(self.tasks)
    

    def get_tasks(self):
        return self.tasksJSON

    def create_task(self,task):
        tasksData = json.loads(self.tasksJSON)
        tasksData.append(task)
        self.tasksJSON = json.dumps(tasksData)
        return self.tasksJSON

    # jsonObject being passed up is a list of dictionaries. need to loop through the list to retrieve all specific values, 
    # do some string edit and then add them to a new list to be output
    def dedupe_json_tags(self,jsonObject):

        tagList = []
        tagList2 = []
        dedupedList = []
        # loop through jsonObject and pull out the tags for each dictionary and save them to a seperate list
        for x in jsonObject:
             tagList.append(x["Tags"])

        # #TagList is a list of dictionaries still. For every dictionary in the list, loop through the individual tags, and then add them to tagList2.
        # #This results in a single dictionary containing every tag keyvalue pair
        for z in tagList:
             for index, item in enumerate(z):
                  tagList2.append(item)

        #dedupes the single dictionary of tags to only contain unique key value pairs
        for i in range(len(tagList2)):
            if tagList2[i] not in tagList2[i+1:]:
                dedupedList.append(tagList2[i])

        tagDict = {"Tags:"}
        tags=[]
        for s in dedupedList:
            name = (str(s["Key"]).replace(".", "-", 1).replace(" ", "-", 1) + ":" + s["Value"]).replace(" ", "-", 1)
            slug = (str(s["Key"]).replace(".", "-", 1).replace(" ", "-", 1) + "-" + str(s["Value"])).replace(" ", "-", 1).lower()
            tempDict = {"name": name, "slug" : slug}
            tags.append(tempDict)

        tagDict = {"Tags": tags}
        outputJson = json.dumps(tagDict, indent=4)
        return outputJson




    def convert_cidr_block(self,cidr):

        network, net_bits = cidr.split('/')
        host_bits = 32 - int(net_bits)
        netmask = socket.inet_ntoa(struct.pack('!I', (1 << 32) - (1 << host_bits)))
        return netmask
    


    def decode_machineName_readable(self,vmName):
        vm = VirtualMachine()

        splitat = len(vmName) - 1
        vm.zone = vmName[splitat:]
        vmName = vmName[:-1]

        splitat = len(vmName) - 3
        vm.region = vmName[splitat:]
        vmName = vmName[:-3]

        splitat = len(vmName) - 3
        vm.env = vmName[splitat:]
        vmName = vmName[:-3]

        splitat = len(vmName) - 2
        vm.numericID = vmName[splitat:]
        vmName = vmName[:-2]

        splitat = len(vmName) - 2
        vm.workload = vmName[splitat:]
        vmName = vmName[:-2]

        vm.machineID = vmName 

        for item in vm.machineIdDict:
            if vm.machineID == vm.machineIdDict[item]:
                vm.machineID = item
        
        for item in vm.workloadDict:
            if vm.workload == vm.workloadDict[item]:
                vm.workload = item

        for item in vm.environmentDict:
            if vm.env == vm.environmentDict[item]:
                vm.env = item
        
        for item in vm.regionDict:
            if vm.region == vm.regionDict[item]:
                vm.region = item

        for item in vm.zoneDict:
            if vm.zone == vm.zoneDict[item]:
                vm.zone = item

        readableMachineName = ( f"VM type: {vm.machineID == '173' and 'Amdocs' or vm.machineID}" + "\n" +
                                "Workload Type: " + vm.workload + "\n" +
                                "ID: " + vm.numericID + "\n" +
                                "Environment: " + vm.env + "\n" +
                                "Region: " + vm.region + "\n" +
                                "Zone:" + vm.zone) 

        return readableMachineName



    def update_task(self, request_task):
        tasksData = json.loads(self.tasksJSON)
        for task in tasksData:
            if task["id"] == request_task['id']:
                task.update(request_task)
                return json.dumps(tasksData);
        return json.dumps({'message': 'task id not found'});

    def delete_task(self, request_task_id):
        tasksData = json.loads(self.tasksJSON)
        for task in tasksData:
            if task["id"] == request_task_id:
                tasksData.remove(task)
                return json.dumps(tasksData);
        return json.dumps({'message': 'task id not found'});

    
