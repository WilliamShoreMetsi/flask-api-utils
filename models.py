"""Application Models"""
import bson, os
from dotenv import load_dotenv
from pymongo import MongoClient
from werkzeug.security import generate_password_hash, check_password_hash

load_dotenv()

#live container connection
#DATABASE_URL=os.environ.get('DATABASE_URL') or 'mongodb://mongodb:27017/myDatabase'

#DATABASE_URL=os.environ.get('DATABASE_URL') or 'mongodb://127.0.0.1:27017/myDatabase'
#print(DATABASE_URL)
#client = MongoClient(DATABASE_URL)
#db = client.get_database()

class VirtualMachine:
    machineID = ""
    workload = ""
    numericID = ""
    env = ""
    region = ""
    zone = ""

    machineIdDict = {
        'App Service Plan' : 'ap',
        'App Services (Web App)' : 'aswe',
        'App Services (Function)' :	'asaf',
        'Application Gateway' :	'ag',
        'Application Insights' : 'ai',
        'Automation Account' : 'aa',
        'Azure Data Factory' :	'df',
        'Azure MySQL' :	'my',
        'Azure SQL'	: 'sq',
        'Azure SQL Data Warehouse' :'dw',
        'Connection' : 'cn',
        'Diagnostics Storage Account' : 'stdi',
        'Event Hub' : 'eh',
        'Gateway (local)' : 'gwl',
        'Gateway (virtual)' : 'gwv',
        'Internal Load Balancer' : 'lb',
        'Key Vault' : 'kv',
        'Log Analytics' : 'la',
        'Network Interface Card' : 'ni',
        'Network Security Group' : 'ns',
        'Public IP' : 'pu',
        'Recovery Services Vault' : 'rs',
        'Redis Cache' : 'rc',
        'Resource Group' : 'rg',
        'Route Table' : 'rt',
        'Scale Set' : 'ss',
        'SQL Alias' : 'sqa',
        'Storage account' : 'st',
        'Subnet' : 'sn',
        'Virtual Machine' : 'vm',
        'Virtual Network' : 'vn',
        'Virtual Network Peering' : 'vnp',
        'Windows Server Failover Cluster' : 'cl',
        'Windows Server Failover Cluster Role' : 'rl'
    }

    workloadDict = {
    "Amdocs DMS server": "dm",
    "Amdocs DMSW server": "sw",
    "Amdocs Online Charging server": "oc",
    "Amdocs Reporting server": "rs",
    "Amdocs Watchdog server": "wd",
    "Anti-virus server": "av",
    "Audit Database": "au",
    "Beats / ATS test Automation server": "be",
    "Biztalk": "bt",
    "Biztalk Receive Location": "br",
    "Biztalk Send Location": "bs",
    "Certificate Authority": "ca",
    "Cognos server" :	"co",
    "Database server" :	"db",
    "Domain Controller" :	"dc",
    "Eptica server" :	"ep",
    "File Server" :	"fs",
    "Fujitsu robotics orchestrator" :	"ro",
    "Fujitsu robotics server" :	"rb",
    "Ginger Test Automation server" :	"gi",
    "IBM Message Queue" :	"mq",
    "Integra" :	"in",
    "Jump box" :	"jb",
    "Kapsch" :	"ka",
    "LDAP server" :	"ld",
    "ILMT Relay" :	"lm",
    "Load Runner Test Automation server" :	"lr",
    "Exchange Servers (Mailbox Servers)" :	"mb",
    "Secure Mail Gateways (ClearSwift)" :	"sm",
    "SFTP server" :	"ft",
    "Taranto":	"ta",
    "Token Database" :	"to",
    "USB hub":	"uh",
    "Web Server" :	"wb"
    }

    environmentDict = {
        'Management 1'	: 'm1a',
        'Hub 1'	: 'h1a',
        'Production 1 North Europe'	: 'p1a',
        'Production 1 West Europe'	: 'p1b',
        'Pre-Production 1 North Europe'	: 'z1a',
        'Pre-Production 1 West Europe'	: 'z1b',
        'Pre-Production 2'	: 'z2a',
        'Pre-Production 3'	: 'Z3a',
        'Development 1'	: 'd1a'
    }

    regionDict = {
        "UK South" :	"uks",
        "UK West" :	"ukw",
        "Europe North" :	"eun",
        "Europe West" :	"euw"
    }

    zoneDict = {
        "Management Network Zone"	: "m",
        "Core Network Zone"	: "c",
        "Internet Demilitarised Network Zone"	: "i",
        "Capita MPLS Demilitarised Network Zone"	: "n",
        "Public PaaS"	: "p"
    }


    def __init__(self):
        return
    
    

class User:
    """User Model"""
    def __init__(self):
        return

    def create(self, name="", email="", password=""):
        """Create a new user"""
        user = self.get_by_email(email)
        if user:
            return
        new_user = db.users.insert_one(
            {
                "name": name,
                "email": email,
                "password": self.encrypt_password(password),
                "active": True
            }
        )
        return self.get_by_id(new_user.inserted_id)

    def get_all(self):
        """Get all users"""
        users = db.users.find({"active": True})
        return [{**user, "_id": str(user["_id"])} for user in users]

    def get_by_id(self, user_id):
        """Get a user by id"""
        user = db.users.find_one({"_id": bson.ObjectId(user_id), "active": True})
        if not user:
            return
        user["_id"] = str(user["_id"])
        user.pop("password")
        return user

    def get_by_email(self, email):
        """Get a user by email"""
        user = db.users.find_one({"email": email, "active": True})
        if not user:
            return
        user["_id"] = str(user["_id"])
        return user

    def update(self, user_id, name=""):
        """Update a user"""
        data = {}
        if name:
            data["name"] = name
        user = db.users.update_one(
            {"_id": bson.ObjectId(user_id)},
            {
                "$set": data
            }
        )
        user = self.get_by_id(user_id)
        return user

    def delete(self, user_id):
        """Delete a user"""
        Books().delete_by_user_id(user_id)
        user = db.users.delete_one({"_id": bson.ObjectId(user_id)})
        user = self.get_by_id(user_id)
        return user

    def disable_account(self, user_id):
        """Disable a user account"""
        user = db.users.update_one(
            {"_id": bson.ObjectId(user_id)},
            {"$set": {"active": False}}
        )
        user = self.get_by_id(user_id)
        return user

    def encrypt_password(self, password):
        """Encrypt password"""
        return generate_password_hash(password)

    def login(self, email, password):
        """Login a user"""
        user = self.get_by_email(email)
        if not user or not check_password_hash(user["password"], password):
            return
        user.pop("password")
        return user
